package com.karishma.todoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.karishma.todoapp.fragment.TodoFragment;

public class MainActivity extends AppCompatActivity {
    FrameLayout fragment_container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment_container = findViewById(R.id.fragment_container);
        if (savedInstanceState == null) {
            Fragment newFragment = new TodoFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, newFragment).commit();
        }
    }
}
