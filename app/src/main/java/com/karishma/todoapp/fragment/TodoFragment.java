package com.karishma.todoapp.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding2.widget.RxTextView;
import com.jakewharton.rxbinding2.widget.TextViewTextChangeEvent;
import com.karishma.todoapp.R;
import com.karishma.todoapp.adapter.ToDoListAdapter;
import com.karishma.todoapp.model.ToDoDataManager;
import com.karishma.todoapp.model.ToDoItems;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.content.Context.MODE_PRIVATE;

public class TodoFragment extends Fragment {
    ToDoListAdapter toDoListAdapter;
    private LinkedList<ToDoItems> toDoItemsList = new LinkedList<>();
    RecyclerView recyclerView;
    EditText searchEditText;
    TextView tvAdd;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ToDoDataManager toDoDataManager;
    public TodoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toDoDataManager = new ToDoDataManager(getContext());
        toDoDataManager.open();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_todo, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        searchEditText = view.findViewById(R.id.todo_search);
        tvAdd = view.findViewById(R.id.tv_add);
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new AddDataFragment();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, newFragment).addToBackStack(null).commit();
            }
        });
        //for search
        compositeDisposable.add(
                RxTextView.textChangeEvents(searchEditText).skipInitialValue().debounce(300, TimeUnit.MICROSECONDS)
                        .distinctUntilChanged().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                        subscribeWith(new DisposableObserver<TextViewTextChangeEvent>() {
                            @Override
                            public void onNext(TextViewTextChangeEvent textViewTextChangeEvent) {
                                toDoListAdapter.getFilter().filter(textViewTextChangeEvent.text());
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        }));

        setRecyclerView();
        loadData();
        return view;
    }
    private void setRecyclerView() {
        toDoListAdapter = new ToDoListAdapter(getActivity(),toDoItemsList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(toDoListAdapter);
    }
    //to show all data while clearing et
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (recyclerView != null) {
                toDoItemsList.clear();
                setRecyclerView();
                loadData();

            }
        }
    }

    private void loadData() {
        Observable<ToDoItems> observable = Observable.fromArray(toDoDataManager.getAllToDoListItem_list().toArray(new ToDoItems[0]));
        compositeDisposable.add(
                observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).
                        subscribeWith(new DisposableObserver<ToDoItems>() {
                            @Override
                            public void onNext(ToDoItems toDoListItem) {
                                toDoItemsList.add(toDoListItem);
                            }
                            @Override
                            public void onError(Throwable e) {
                            }
                            @Override
                            public void onComplete() {
                                toDoListAdapter.notifyDataSetChanged();
                            }
                        }));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
