package com.karishma.todoapp.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.karishma.todoapp.R;
import com.karishma.todoapp.model.ToDoDataManager;
import com.karishma.todoapp.model.ToDoItems;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddDataFragment extends Fragment {
    private ToDoDataManager dbManager;
    EditText et_title,et_desc;
    TextView tv_cancel,tv_done;
    private int userId;
    public AddDataFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_data, container, false);
        et_title = view.findViewById(R.id.et_title);
        et_desc = view.findViewById(R.id.et_desc);
        tv_cancel = view.findViewById(R.id.tv_cancel);
        tv_done = view.findViewById(R.id.tv_done);
        dbManager = new ToDoDataManager(getActivity());
        dbManager.open();
        tv_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((et_title.getText().toString()).equals("") || (et_desc.getText().toString()).equals("")) {

                    Toast.makeText(getActivity(), "Please, add a content", Toast.LENGTH_SHORT).show();

                }
                 else {


                    dbManager.addToDoListItem(new ToDoItems(
                            et_title.getText().toString(),
                            et_desc.getText().toString(),
                            userId));
                    TodoFragment todoFragment = new TodoFragment ();
                    getFragmentManager().beginTransaction().replace(R.id.fragment_container, todoFragment).commit();

                }

            }
        });
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TodoFragment todoFragment = new TodoFragment ();
                getFragmentManager().beginTransaction().replace(R.id.fragment_container, todoFragment).commit();

            }
        });
        return view;
    }
}
