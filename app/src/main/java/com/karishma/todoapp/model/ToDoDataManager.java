package com.karishma.todoapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;



public class ToDoDataManager {


    private DatabaseHandler databaseHelper;

    private Context context;

    private SQLiteDatabase database;

    // ToDoListItem_list table name
    private static final String TABLE_ToDoListItem_LIST = "ToDoListItem_list";
    // ToDoListItem_list Table Columns names
    private static final String KEY_ToDoListItem_LIST_NAMES_LIST_ID = "ToDoListItem_list_id";
    private static final String KEY_ToDoListItem_LIST_NAME = "ToDoListItem_list_name";
    private static final String KEY_ToDoListItem_LIST_DESCRIPTION = "ToDoListItem_list_description";
    private static final String KEY_ToDoListItem_CHECKED = "ToDoListItem_list_checked";
    private static final String KEY_ToDo_Id = "ToDo_id";


    public ToDoDataManager(Context c) {
        context = c;
    }

    public ToDoDataManager open() throws SQLException {
        databaseHelper = new DatabaseHandler(context);
        database = databaseHelper.getWritableDatabase();
        return this;
    }


    // Adding new ToDoListItem
    public void addToDoListItem(ToDoItems ToDoListItem) {
        ContentValues values = new ContentValues();
        values.put(KEY_ToDoListItem_LIST_NAME, ToDoListItem.getToDoListItemName()); // ToDoListItem Name
        values.put(KEY_ToDoListItem_LIST_DESCRIPTION, ToDoListItem.getToDoListItemDescription()); // ToDoListItem Description
        values.put(KEY_ToDo_Id, ToDoListItem.getUserId());
        values.put(KEY_ToDoListItem_CHECKED,ToDoListItem.isChecked());


        // Inserting Row
        database.insert(TABLE_ToDoListItem_LIST, null, values);


    }


    // Getting All ToDoListItem_list
    public ArrayList<ToDoItems> getAllToDoListItem_list() {
        ArrayList<ToDoItems> ToDoListItemList = new ArrayList<ToDoItems>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ToDoListItem_LIST;


        Cursor cursor = database.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ToDoItems ToDoListItem = new ToDoItems();
               // ToDoListItem.setToDoListItemId(Integer.parseInt(cursor.getString(0)));
                ToDoListItem.setUserId(Integer.parseInt(cursor.getString(0)));
                ToDoListItem.setToDoListItemName(cursor.getString(1));
                ToDoListItem.setToDoListItemDescription(cursor.getString(2));
                ToDoListItem.setChecked(Boolean.parseBoolean(cursor.getString(3)));


                // Adding ToDoListItem to list
                ToDoListItemList.add(ToDoListItem);
            } while (cursor.moveToNext());
        }

        // return ToDoListItem list
        return ToDoListItemList;
    }


    // Updating single ToDoListItem
    public int updateToDoListItem(ToDoItems ToDoListItem) {
        // SQLiteDatabase database = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ToDoListItem_LIST_NAME, ToDoListItem.getToDoListItemName());
        values.put(KEY_ToDoListItem_LIST_DESCRIPTION, ToDoListItem.getToDoListItemDescription());
        values.put(KEY_ToDoListItem_CHECKED, ToDoListItem.isChecked());

        // updating row
        int result = database.update(TABLE_ToDoListItem_LIST, values, KEY_ToDoListItem_LIST_NAMES_LIST_ID + " = ?",
        new String[]{String.valueOf(ToDoListItem.getToDoListItemId())});
      /* int result = database.update(TABLE_ToDoListItem_LIST, values, KEY_ToDoListItem_CHECKED + " = ?",
                new String[]{String.valueOf(ToDoListItem.isChecked())});*/



      //  database.close();

        return result;


    }

    // Deleting single ToDoListItem
    public void deleteToDoListItem(ToDoItems ToDoListItem) {

        database.delete(TABLE_ToDoListItem_LIST, KEY_ToDoListItem_LIST_NAMES_LIST_ID + " = ?",
                new String[]{String.valueOf(ToDoListItem.getToDoListItemId())});
       // database.close();
    }




}