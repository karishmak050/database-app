package com.karishma.todoapp.model;

public class ToDoItems {
    private int userId;
    private String toDoListItemName;
    private String toDoListItemDescription;
    private int toDoListItemId;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public ToDoItems() {
    }

    public ToDoItems(String toDoListItemName, String toDoListItemDescription, int userId) {
        this.toDoListItemName = toDoListItemName;
        this.toDoListItemDescription = toDoListItemDescription;
        this.userId = userId;
    }



    public int getToDoListItemId() {
        return toDoListItemId;
    }

    public void setToDoListItemId(int toDoListItemId) {
        this.toDoListItemId = toDoListItemId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToDoListItemName() {
        return toDoListItemName;
    }

    public void setToDoListItemName(String toDoListItemName) {
        this.toDoListItemName = toDoListItemName;
    }

    public String getToDoListItemDescription() {
        return toDoListItemDescription;
    }

    public void setToDoListItemDescription(String toDoListItemDescription) {
        this.toDoListItemDescription = toDoListItemDescription;
    }
}
