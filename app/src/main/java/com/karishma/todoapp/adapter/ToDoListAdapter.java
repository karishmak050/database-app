package com.karishma.todoapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karishma.todoapp.R;
import com.karishma.todoapp.fragment.TodoFragment;
import com.karishma.todoapp.model.ToDoDataManager;
import com.karishma.todoapp.model.ToDoItems;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ToDoListAdapter extends RecyclerView.Adapter<ToDoListAdapter.ViewHolder> implements Filterable {
    private Context context;
    private LinkedList<ToDoItems> resultLists;
    private LinkedList<ToDoItems> todolistFiltered;
    int row_index;
    ToDoDataManager dbManager;

    public ToDoListAdapter(Context context, LinkedList<ToDoItems> resultLists) {
        this.context = context;
        this.resultLists = resultLists;
        this.todolistFiltered = resultLists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_todo, parent, false);
        dbManager = new ToDoDataManager(context);
        dbManager.open();
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        row_index = position;
        final ToDoItems item = resultLists.get(position);
        holder.tv_title.setText(item.getToDoListItemName());
        holder.tv_desc.setText(item.getToDoListItemDescription());


        holder.checkbox.setOnCheckedChangeListener(null);
        holder.checkbox.setChecked(item.isChecked());
        holder.checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                resultLists.get(position).setChecked(true);
                resultLists.addLast(resultLists.get(position));
                resultLists.remove(position);
                ToDoItems toDoItems = resultLists.getLast();
                dbManager.updateToDoListItem(toDoItems);
                notifyDataSetChanged();



            }
        });
    }

    @Override
    public int getItemCount() {
        return todolistFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_title, tv_desc;
        CheckBox checkbox;
        CardView ll_layout;

        public ViewHolder(final View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            checkbox = itemView.findViewById(R.id.checkbox);
            ll_layout = itemView.findViewById(R.id.ll_layout);

        }


    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    todolistFiltered = resultLists;
                } else {
                    LinkedList<ToDoItems> filteredList = new LinkedList<>();
                    for (ToDoItems item : resultLists) {

                        if (item.getToDoListItemName().toLowerCase().contains(charString.toLowerCase())
                                || item.getToDoListItemDescription().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(item);
                        }
                    }
                    todolistFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = todolistFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                todolistFiltered = (LinkedList<ToDoItems>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}